unit uInterf;

interface
uses
  Graph, Crt;

procedure MenuCaption;
procedure GameCaption;
procedure Game2Caption;
procedure ShowScreen;
procedure Menu(ItemNo: Integer);
procedure ShowOptions;
procedure ShowHelp;

var
  SoundEnable, Clever: Boolean;

implementation

procedure MenuCaption;
begin
  SetFillStyle(1,0);
  Bar(0,21,GetMaxX,GetMaxY-41);
  SetFillStyle(1,1);
  Bar(0,0,GetMaxX,20);
  SetColor(15);
  OutTextXY(20,7,'Main Menu');
  OutTextXY(GetMaxX-165,7,'Philip''s SeaBattle');
  Bar(0,GetMaxY-40,GetMaxX,GetMaxY);
  OutTextXY(20,GetMaxY-32,'F1: Help             '+#24+'/'+
    #25+': Cursor Up/Down');
  OutTextXY(20,GetMaxY-12,'                     Enter: Select');
end;

procedure GameCaption;
begin
  SetFillStyle(1,0);
  Bar(0,21,GetMaxX,GetMaxY-41);
  SetFillStyle(1,1);
  Bar(0,0,GetMaxX,20);
  SetColor(15);
  OutTextXY(GetMaxX-165,7,'Philip''s SeaBattle');
  Bar(0,GetMaxY-40,GetMaxX,GetMaxY);
  OutTextXY(20,GetMaxY-32,'Esc: Main Menu       '+#24+'/'+#25+'/'+#26+'/'+
    #27+': Cursor Up/Down/Right/Left');
  OutTextXY(20,GetMaxY-12,'Enter: Accept        '+
    'Space: Place Ship                A: Automatic Position');
end;

procedure Game2Caption;
begin
  SetFillStyle(1,0);
  Bar(0,21,GetMaxX,GetMaxY-41);
  SetFillStyle(1,1);
  Bar(0,0,GetMaxX,20);
  SetColor(15);
  OutTextXY(GetMaxX-165,7,'Philip''s SeaBattle');
  Bar(0,GetMaxY-40,GetMaxX,GetMaxY);
  OutTextXY(20,GetMaxY-32,'Esc: Main Menu       '+#24+'/'+#25+'/'+#26+'/'+
    #27+': Cursor Up/Down/Left/Right');
  OutTextXY(20,GetMaxY-12,'                     Enter, Space: Fire');
end;

procedure ShowScreen;
begin
  SetFillStyle(1,0);
  Bar(0,0,GetMaxX,GetMaxY);
  SetColor(4);
  SetTextJustify(CenterText,CenterText);
  SetTextStyle(DefaultFont,HorizDir,2);
  OutTextXY(GetMaxX div 2,GetMaxY div 2 - 50,'Philip''s SeaBattle');
  SetTextStyle(DefaultFont,HorizDir,1);
  OutTextXY(GetMaxX div 2,GetMaxY div 2 + 200,'Loading...');
  SetTextJustify(LeftText,TopText);
  Delay(64000);
  Delay(64000);
  Delay(64000);
end;

procedure Menu(ItemNo: Integer);
var
  x0, y0: Integer;

begin
  x0:=GetMaxX div 2 - 70;
  y0:=GetMaxY div 2 - 80;
  case ItemNo of
    0:
      begin
        SetFillStyle(1,0);
        Bar(x0,y0+30,x0+140,y0+30+18);
        Bar(x0,y0+60,x0+140,y0+60+18);
        Bar(x0,y0+90,x0+140,y0+90+18);
        SetFillStyle(1,8);
        Bar(x0,y0,x0+140,y0+18);
      end;
    1:
      begin
        SetFillStyle(1,0);
        Bar(x0,y0,x0+140,y0+18);
        Bar(x0,y0+60,x0+140,y0+60+18);
        Bar(x0,y0+90,x0+140,y0+90+18);
        SetFillStyle(1,8);
        Bar(x0,y0+30,x0+140,y0+30+18);
      end;
    2:
      begin
        SetFillStyle(1,0);
        Bar(x0,y0,x0+140,y0+18);
        Bar(x0,y0+30,x0+140,y0+30+18);
        Bar(x0,y0+90,x0+140,y0+90+18);
        SetFillStyle(1,8);
        Bar(x0,y0+60,x0+140,y0+60+18);
      end;
    3:
      begin
        SetFillStyle(1,0);
        Bar(x0,y0,x0+140,y0+18);
        Bar(x0,y0+30,x0+140,y0+30+18);
        Bar(x0,y0+60,x0+140,y0+60+18);
        SetFillStyle(1,8);
        Bar(x0,y0+90,x0+140,y0+90+18);
      end;
  end;
  SetTextJustify(CenterText,CenterText);
  SetColor(4);
  OutTextXY(GetMaxX div 2,GetMaxY div 2 - 70,'Start New Game');
  OutTextXY(GetMaxX div 2,GetMaxY div 2 - 40,'Options');
  OutTextXY(GetMaxX div 2,GetMaxY div 2 - 10,'Help');
  OutTextXY(GetMaxX div 2,GetMaxY div 2 + 20,'Exit');
  SetTextJustify(LeftText,TopText);
  SetColor(8);
  OutTextXY(GetMaxX - 110,GetMaxY - 60,'v. 1.0.0.20');
  OutTextXY(20,GetMaxY - 90,'Author:   Phil Tsarik');
  OutTextXY(20,GetMaxY - 75,'E-Mail:   philip-s@yandex.ru');
  OutTextXY(20,GetMaxY - 60,'Web:      http://philip-s.narod.ru');
  SetColor(15);
end;

procedure ShowOptions;
var
  Key, Key2: Char;
  Item: Integer;

procedure OptionsCaption;
begin
  SetFillStyle(1,0);
  Bar(0,21,GetMaxX,GetMaxY-41);
  SetFillStyle(1,1);
  Bar(0,0,GetMaxX,20);
  SetColor(15);
  OutTextXY(20,7,'Options');
  OutTextXY(GetMaxX-165,7,'Philip''s SeaBattle');
  Bar(0,GetMaxY-40,GetMaxX,GetMaxY);
  OutTextXY(20,GetMaxY-32,'Esc: Main Menu       '+#24+'/'+#25+
    ': Cursor Up/Down');
  OutTextXY(20,GetMaxY-12,'                     Enter, Space: Change Value');
end;

procedure OptionsItems(ItemNo: Integer);
var
  x0, y0: Integer;
begin
  x0:=20;
  y0:=34;
  case ItemNo of
    0:
      begin
        SetFillStyle(1,0);
        Bar(x0,y0+30,x0+200,y0+30+18);
        SetFillStyle(1,8);
        Bar(x0,y0,x0+200,y0+18);
      end;
    1:
      begin
        SetFillStyle(1,0);
        Bar(x0,y0,x0+200,y0+18);
        SetFillStyle(1,8);
        Bar(x0,y0+30,x0+200,y0+30+18);
      end;
  end;
  SetColor(7);
  OutTextXY(20,40,#16+' Computer');
  OutTextXY(20,70,#16+' Sound');
  SetColor(15);
  if Clever then
    OutTextXY(20,40,'               [Clever]')
  else
    OutTextXY(20,40,'               [Silly]');
  if SoundEnable then
    OutTextXY(20,70,'               [Enable]')
  else
    OutTextXY(20,70,'               [Disable]');
end;

begin
  OptionsCaption;
  Item:=0;
  OptionsItems(Item);
  while True do
  begin
    Key:=ReadKey;
    case Key of
      #0:
        begin
          Key2:=ReadKey;
          case Key2 of
            #72:
              begin
                Dec(Item);
                if Item<0 then
                  Item:=1;
                OptionsItems(Item);
              end;
            #80:
              begin
                Inc(Item);
                if Item>1 then Item:=0;
                OptionsItems(Item);
              end;
          end;
        end;
      #13, #32:
        case Item of
          0:
            begin
              Clever:=not Clever;
              OptionsItems(Item);
            end;
          1:
            begin
              SoundEnable:=not SoundEnable;
              OptionsItems(Item);
            end;
        end;
      #27:
        Exit;
    end;
  end;
end;

procedure ShowHelp;
var Key: Char;

procedure HelpCaption;
begin
  SetFillStyle(1,0);
  Bar(0,21,GetMaxX,GetMaxY-41);
  SetFillStyle(1,1);
  Bar(0,0,GetMaxX,20);
  SetColor(15);
  OutTextXY(20,7,'Help');
  OutTextXY(GetMaxX-165,7,'Philip''s SeaBattle');
  Bar(0,GetMaxY-40,GetMaxX,GetMaxY);
  OutTextXY(20,GetMaxY-32,'Esc: Main Menu');
end;

begin
  HelpCaption;
  SetColor(7);
  OutTextXY(20,40,'RULES');
  OutTextXY(20,70,'You and your opponent have fleet that consists of');
  OutTextXY(20,85,'  one aircraft carrier (4 cells),');
  OutTextXY(20,100,'  two battleships (3 cells),');
  OutTextXY(20,115,'  three submarines (2 cells) and');
  OutTextXY(20,130,'  four torpedo boats (1 cell).');
  OutTextXY(20,160,
    'Your have to destroy the opponent''s fleet and save yours.');
  while True do
  begin
    Key:=ReadKey;
    if Key = #27 then
      Exit;
  end;
end;

end.