unit uGame;

interface
uses
  Graph, Crt, uInterf;

type
  TField = Object
    Pole: Array [0..9,0..9] of ShortInt;
    x0, y0: Integer;
    Hide: Boolean;
    id: Byte;
    end;

procedure InitField(var F: TField);
procedure DrawShips(F: TField);
procedure DrawGrid(F: TField);
procedure AutoPlace(var F: TField);
procedure PlaceCompShips;
function CheckShips: Boolean;
function PlaceShips(F: TField): Boolean;
function GameOver(F: TField): Boolean;
function Fire(x, y: Integer; var F: TField): Boolean;
function HumTurn: Boolean;
procedure CompTurn;
procedure StartGame;
procedure InitGame;

var
  Field, CompField: TField;
  LastX, LastY: Integer;
  Len: Byte;
  CompState: Byte;
  Sx, Sy, Dx, Dy: Integer;
  ExShip: Boolean;
  GameEnd, Looser: Boolean;


implementation


procedure InitField(var F: TField);
var
  i, j: Integer;
begin
  for i:=0 to 9 do
    for j:=0 to 9 do
      F.Pole[i,j]:=-1;
end;

procedure DrawShips(F: TField);
var
  i, j: Integer;
begin
  for i:=0 to 9 do
    for j:=0 to 9 do
      case F.Pole[i,j] of
        -1:
          begin
            SetFillStyle(1,3);
            Bar(i*20+F.x0+2,j*20+F.y0+2,i*20+F.x0+18,j*20+F.y0+18);
          end;
        0:
          begin
            SetFillStyle(1,3);
            Bar(i*20+F.x0+2,j*20+F.y0+2,i*20+F.x0+18,j*20+F.y0+18);
            if not F.Hide then
            begin
              SetFillStyle(1,8);
              Bar(i*20+F.x0+2,j*20+F.y0+2,i*20+F.x0+18,j*20+F.y0+18);
            end;
          end;
        1:
          begin
            SetFillStyle(1,12);
            Bar(i*20+F.x0+2,j*20+F.y0+2,i*20+F.x0+18,j*20+F.y0+18);
          end;
        2:
          begin
            SetFillStyle(1,3);
            Bar(i*20+F.x0+2,j*20+F.y0+2,i*20+F.x0+18,j*20+F.y0+18);
            SetFillStyle(1,1);
            Bar(i*20+F.x0+8,j*20+F.y0+8,i*20+F.x0+12,j*20+F.y0+12);
          end;
      end;
end;

procedure DrawGrid(F: TField);
var
  i, j, x, y: Integer;
  Ch: Char;
  St: String;
begin
  SetColor(7);
  case F.id of
    1: OutTextXY(F.x0,F.y0-30,'Your sea');
    2: OutTextXY(F.x0,F.y0-30,'Sea of the computer');
  end;
  SetColor(15);
  SetFillStyle(1,3);
  for i:=0 to 9 do
    for j:=0 to 9 do
    begin
      x:=i*20+F.x0;
      y:=j*20+F.y0;
      Bar(x,y,x+20,y+20);
      Rectangle(x,y,x+20,y+20);
    end;
  for i:=0 to 9 do
  begin
    Ch:=Chr(i+97);
    x:=i*20+F.x0;
    OutTextXY(x+7,F.y0-10,Ch);
  end;
  for i:=1 to 10 do
  begin
    Str(i,St);
    y:=i*20+F.y0;
    OutTextXY(F.x0-20,y-12,St);
  end;
  DrawShips(F);
end;

procedure AutoPlace(var F: TField);
var
  n, m, i: Integer;
  x, y, kx, ky: Integer;
  Bool: Boolean;

function FreeCell(x, y: Integer; F: TField): Boolean;
const
  d: Array[1..8,1..2] of Integer =
      ((0,1),(1,0),(0,-1),(-1,0),(1,1),(-1,1),(1,-1),(-1,-1));
var
  i: Integer;
  dx, dy: Integer;
begin
  if(x >= 0)and(x <= 9)and(y >= 0)and(y <= 9)and(F.Pole[x,y] = -1)then
  begin
    for i := 1 to 8 do
    begin
      dx:=x + d[i,1];
      dy:=y + d[i,2];
      if(dx >= 0)and(dx <= 9)and(dy >= 0)and(dy <= 9)
        and(F.Pole[dx,dy] <> -1)then
      begin
        FreeCell:=False;
        Exit;
      end;
    end;
    FreeCell:=True;
  end
  else
    FreeCell:=False;
end;

begin
  InitField(F);
  for n:=3 downto 0 do
    for m:=0 to 3 - n do
      repeat
        x:=Random(10);
        y:=Random(10);
        kx:=Random(2);
        if kx = 0 then
          ky:=1
        else
          ky:=0;
        Bool:=True;
        for i:=0 to n do
          if not FreeCell(x+kx*i,y+ky*i,F) then Bool:=False;
        if Bool then
          for i:=0 to n do
            F.Pole[x+kx*i,y+ky*i]:=0;
      until Bool;
end;

procedure PlaceCompShips;
begin
  AutoPlace(CompField);
end;

function CheckShips: Boolean;
var
  x, y, n, m, k: Integer;
  Ar: Array[0..9,0..9] of Integer;
  Sh: Array[1..4] of Integer;

function Check(x,y:Integer): Boolean;
begin
  Check:=False;
  if(x<=9)and(y<=9)and(x>=0)and(y>=0)and(Field.Pole[x,y]=0)then
    Check:=True;
end;

begin
  CheckShips:=True;
  for x:=0 to 9 do
    for y:=0 to 9 do
      Ar[x,y]:=Field.Pole[x,y];
  for x:=0 to 9 do
    for y:=0 to 9 do
      if Ar[x,y]>-1 then
      begin
        n:=x-1;
        m:=y-1;
        if Check(n,m) then
        begin
          CheckShips:=False;
          Exit;
        end;
        n:=x+1;
        if Check(n,m) then
        begin
          CheckShips:=False;
          Exit;
        end;
        m:=y+1;
        if Check(n,m) then
        begin
          CheckShips:=False;
          Exit;
        end;
        n:=x-1;
        m:=y+1;
        if Check(n,m) then
        begin
          CheckShips:=False;
          Exit;
        end;
      end;
  for k:=1 to 4 do
    Sh[k]:=0;
  for x:=0 to 9 do
    for y:=0 to 9 do
      if (Ar[x,y]=0)and(not Check(x,y-1))and(not Check(x,y+1))then
      begin
        m:=x;
        k:=0;
        repeat
          k:=k+1;
          Ar[m,y]:=-1;
          m:=m+1;
          if m>9 then m:=9;
        until Ar[m,y]=-1;
        if k>4 then
        begin
          CheckShips:=False;
          Exit;
        end
        else
          Sh[k]:=Sh[k]+1;
      end;
  for x:=0 to 9 do
    for y:=0 to 9 do
      if(Ar[x,y]=0)and(not Check(x-1,y))and(not Check(x+1,y))then
      begin
        m:=y;
        k:=0;
        repeat
          k:=k+1;
          Ar[x,m]:=-1;
          m:=m+1;
          if m>9 then m:=9;
        until Ar[x,m]=-1;
        if k>4 then
        begin
          CheckShips:=False;
          Exit;
        end
        else
          Sh[k]:=Sh[k]+1;
      end;
  if(Sh[1]<>4)or(Sh[2]<>3)or(Sh[3]<>2)or(Sh[4]<>1)then
    CheckShips:=False;
end;

function PlaceShips(F: TField): Boolean;
var
  Key, Key2: Char;
  x, y: Integer;
begin
  x:=0;
  y:=0;
  while True do
  begin
    Rectangle(x*20+F.x0+2,y*20+F.y0+2,x*20+F.x0+18,y*20+F.y0+18);
    Key:=ReadKey;
    DrawShips(Field);
    case Key of
      #0:
        begin
          Key2:=ReadKey;
          case Key2 of
            #72:         { [Up] }
              begin
                Dec(y);
                if y<0 then y:=9;
                Rectangle(x*20+F.x0+2,y*20+F.y0+2,x*20+F.x0+18,y*20+F.y0+18);
              end;
            #75:         { [Left] }
              begin
                Dec(x);
                if x<0 then x:=9;
                Rectangle(x*20+F.x0+2,y*20+F.y0+2,x*20+F.x0+18,y*20+F.y0+18);
              end;
            #77:         { [Right] }
              begin
                Inc(x);
                if x>9 then x:=0;
                Rectangle(x*20+F.x0+2,y*20+F.y0+2,x*20+F.x0+18,y*20+F.y0+18);
              end;
            #80:         { [Down] }
              begin
                Inc(y);
                if y>9 then y:=0;
                Rectangle(x*20+F.x0+2,y*20+F.y0+2,x*20+F.x0+18,y*20+F.y0+18);
              end;
          end;
        end;
      #13:      { [Enter] }
        begin
          if CheckShips then
          begin
            PlaceShips:=True;
            Exit;
          end
          else
          begin
            SetFillStyle(1,4);
            Bar(GetMaxX div 2 - 160,GetMaxY div 2 - 10,GetMaxX div 2 + 160,
              GetMaxY div 2 + 30);
            SetTextJustify(CenterText,CenterText);
            OutTextXY(GetMaxX div 2,GetMaxY div 2,
              'Error. Wrong arrangement of the ships');
            OutTextXY(GetMaxX div 2,GetMaxY div 2 + 20,'[ Ok ]');
            SetTextJustify(LeftText,TopText);
            ReadKey;
            GameCaption;
            DrawGrid(Field);
          end;
        end;
      #27:      { [Esc] }
        begin
          PlaceShips:=False;
          Exit;
        end;
      #32:      { [Space] }
        begin
          if Field.Pole[x,y] = -1 then
            Field.Pole[x,y]:=0
          else
            Field.Pole[x,y]:=-1;
          DrawShips(Field);
        end;
      #97,#65:  { [a] }
        begin
          AutoPlace(Field);
          DrawShips(Field);
        end;
    end;
  end;
end;

function GameOver(F: TField): Boolean;
var
  i, j: Integer;
begin
  GameOver:=True;
  GameEnd:=True;
  for i:=0 to 9 do
    for j:=0 to 9 do
      if F.Pole[i,j]=0 then
      begin
        GameOver:=False;
        GameEnd:=False;
        Exit;
      end;
  GameEnd:=True;
end;

function Fire(x, y: Integer; var F: TField): Boolean;
begin
  case F.Pole[x,y] of
    -1:
      begin
        Circle(x*20+F.x0+10,y*20+F.y0+10,2);
        Delay(32000);
        DrawShips(F);
        Circle(x*20+F.x0+10,y*20+F.y0+10,4);
        Delay(32000);
        DrawShips(F);
        Circle(x*20+F.x0+10,y*20+F.y0+10,6);
        Delay(32000);
        F.Pole[x,y]:=2;
        DrawShips(F);
        Fire:=False;

        if SoundEnable then
        begin
          Sound(250);
          Delay(16000);
          Sound(200);
          Delay(16000);
          Sound(150);
          Delay(16000);
          NoSound;
        end;
      end;
    0:
      begin
        SetFillStyle(1,12);
        Bar(x*20+F.x0+8,y*20+F.y0+8,x*20+F.x0+12,y*20+F.y0+12);
        Delay(32000);
        Bar(x*20+F.x0+6,y*20+F.y0+6,x*20+F.x0+14,y*20+F.y0+14);
        Delay(32000);
        Bar(x*20+F.x0+4,y*20+F.y0+4,x*20+F.x0+16,y*20+F.y0+16);
        Delay(32000);
        F.Pole[x,y]:=1;
        DrawShips(F);
        Fire:=True;

        if SoundEnable then
        begin
          Sound(200);
          Delay(16000);
          Sound(150);
          Delay(28000);
          Sound(100);
          Delay(64000);
          NoSound;
        end;
      end;
  end;
end;

function HumTurn: Boolean;
var
  Key, Key2: Char;
  x, y: Integer;
  Kill: Boolean;
begin
  x:=LastX;
  y:=LastY;
  Kill:=True;
  HumTurn:=True;
  while Kill and not GameEnd do
  begin
    Circle(x*20+CompField.x0+10,y*20+CompField.y0+10,5);
    Key:=ReadKey;
    DrawShips(CompField);
    case Key of
      #0:
        begin
          Key2:=ReadKey;
          case Key2 of
            #72:         { [Up] }
              begin
                Dec(y);
                if y<0 then y:=9;
                Circle(x*20+CompField.x0+10,y*20+CompField.y0+10,5);
              end;
            #75:         { [Left] }
              begin
                Dec(x);
                if x<0 then x:=9;
                Circle(x*20+CompField.x0+10,y*20+CompField.y0+10,5);
              end;
            #77:         { [Right] }
              begin
                Inc(x);
                if x>9 then x:=0;
                Circle(x*20+CompField.x0+10,y*20+CompField.y0+10,5);
              end;
            #80:         { [Down] }
              begin
                Inc(y);
                if y>9 then y:=0;
                Circle(x*20+CompField.x0+10,y*20+CompField.y0+10,5);
              end;
          end;
        end;
      #27:
        begin
          HumTurn:=False;
          Exit;
        end;
      #13, #32:
        begin
          LastX:=x;
          LastY:=y;
          Kill:=Fire(x,y,CompField);
          if GameOver(CompField) then
          begin
            GameEnd:=True;
            Looser:=False;
          end;
        end;
    end;
  end;
end;

procedure CompTurn;
var
  x, y: Integer;
  Kill: Boolean;

function CountDestrShips(x, y: Integer; F: TField): Integer;
const
  d: Array[1..8,1..2] of Integer =
      ((0,1),(1,0),(0,-1),(-1,0),(1,1),(-1,1),(1,-1),(-1,-1));
var
  i, dx, dy, n: Integer;
begin
  if(x >= 0)and(x <= 9)and(y >= 0)and(y <= 9)and(F.Pole[x,y] <> 1)then
  begin
    n:=0;
    for i := 1 to 8 do
    begin
      dx:=x + d[i,1];
      dy:=y + d[i,2];
      if(dx >= 0)and(dx <= 9)and(dy >= 0)and(dy <= 9)and(F.Pole[dx,dy] = 1)then
        Inc(n);
    end;
    CountDestrShips:=n;
  end
  else
    CountDestrShips:=8;
end;

procedure BeginSearch;
begin
  ExShip:=False;
  repeat
    x:=Random(10);
    y:=Random(10);
  until (Field.Pole[x,y]<>2)and(CountDestrShips(x,y,Field)=0);
  Kill:=Fire(x,y,Field);
  if Kill and Clever then
  begin
    Len:=1;
    Sx:=x;
    Sy:=y;
    CompState:=1;
  end
  else
    CompState:=0;
end;

procedure BeginBombarding;
var
  Nothing: Boolean;
begin
  Kill:=False;
  Nothing:=False;
  if(Sx>=0)and(Sx<=9)and(Sy-1>=0)and(Sy-1<=8)and((Field.Pole[Sx,Sy-1]=-1)
    or(Field.Pole[Sx,Sy-1]=0))and(CountDestrShips(Sx,Sy-1,Field)=1)then
  begin
    x:=Sx;
    y:=Sy-1;
    Kill:=Fire(x,y,Field);
  end
  else
    if(Sx+1>=1)and(Sx+1<=9)and(Sy>=0)and(Sy<=9)and((Field.Pole[Sx+1,Sy]=-1)
      or(Field.Pole[Sx+1,Sy]=0))and(CountDestrShips(Sx+1,Sy,Field)=1)then
    begin
      x:=Sx+1;
      y:=Sy;
      Kill:=Fire(x,y,Field);
    end
    else
      if(Sx>=0)and(Sx<=9)and(Sy+1>=1)and(Sy+1<=9)and((Field.Pole[Sx,Sy+1]=-1)
        or(Field.Pole[Sx,Sy+1]=0))and(CountDestrShips(Sx,Sy+1,Field)=1)then
      begin
        x:=Sx;
        y:=Sy+1;
        Kill:=Fire(x,y,Field);
      end
      else
        if(Sx-1>=0)and(Sx-1<=8)and(Sy>=0)and(Sy<=9)and((Field.Pole[Sx-1,Sy]=-1)
          or(Field.Pole[Sx-1,Sy]=0))and(CountDestrShips(Sx-1,Sy,Field)=1)then
        begin
          x:=Sx-1;
          y:=Sy;
          Kill:=Fire(x,y,Field);
        end
        else
        begin
          CompState:=0;
          Nothing:=True;
        end;
  if Kill then
  begin
    Len:=2;
    Dx:=x-Sx;
    Dy:=y-Sy;
    Sx:=x;
    Sy:=y;
    CompState:=2;
  end;
  if Nothing then
    BeginSearch;
end;

procedure BeginDestroy;
begin
  x:=Sx;
  y:=Sy;
  repeat
    x:=x+Dx;
    y:=y+Dy;
    if((x>9)and(Dx=1))or((x<0)and(Dx=-1))or((y>9)and(Dy=1))or((y<0)and(Dy=-1))
      or(Field.Pole[x,y]=2)or((CountDestrShips(x,y,Field)>1)
      and(Field.Pole[x,y]<>1))then
    begin
      Dx:=-Dx;
      Dy:=-Dy;
      x:=x+Dx+Dx;
      y:=y+Dy+Dy;
      if ExShip then
      begin
        CompState:=0;
        Exit;
      end;
      ExShip:=True;
    end;
  until (Field.Pole[x,y]<>1);
  Kill:=Fire(x,y,Field);
  if not Kill then
  begin
    Dx:=-Dx;
    Dy:=-Dy;
    if ExShip then
    begin
      CompState:=0;
      Exit;
    end;
    ExShip:=True;
  end
  else
    Inc(Len);
  Sx:=x;
  Sy:=y;
  if Len = 4 then
    CompState:=0;
end;

begin
  Kill:=True;
  while Kill and not GameEnd do
  begin
    Delay(64000);
    Delay(64000);
    case CompState of
      0: BeginSearch;
      1: BeginBombarding;
      2: BeginDestroy;
    end;
    if GameOver(Field) then
      Looser:=True;
  end;
end;

procedure StartGame;
var
  Hum, Stop: Boolean;
begin
  case Random(2) of
    0: Hum:=True;
    1: Hum:=False;
    end;
  Stop:=False;
  while not Stop and not GameEnd do
  begin
    if Hum then
      Stop:=not HumTurn
    else
      CompTurn;
    Hum:=not Hum;
  end;
end;

procedure InitGame;
var Key: Char;
begin
  GameEnd:=False;
  Looser:=False;
  GameCaption;
  Randomize;
  InitField(Field);
  DrawGrid(Field);
  InitField(CompField);
  if PlaceShips(Field) then
  begin
    Game2Caption;
    DrawGrid(Field);
    LastX:=4;
    LastY:=4;
    DrawGrid(CompField);
    PlaceCompShips;
    StartGame;
  end;
  if GameEnd then
  begin
    repeat
      if Looser then
      begin
        SetFillStyle(1,4);
        Bar(GetMaxX div 2 - 130,GetMaxY div 2 - 10,GetMaxX div 2 + 130,
          GetMaxY div 2 + 30);
        SetTextJustify(CenterText,CenterText);
        OutTextXY(GetMaxX div 2,GetMaxY div 2,'Your fleet is sunk! You lose');
        OutTextXY(GetMaxX div 2,GetMaxY div 2 + 20,'[ Ok ]');
        SetTextJustify(LeftText,TopText);
      end
      else
      begin
        SetFillStyle(1,4);
        Bar(GetMaxX div 2 - 220,GetMaxY div 2 - 10,GetMaxX div 2 + 220,
          GetMaxY div 2 + 30);
        SetTextJustify(CenterText,CenterText);
        OutTextXY(GetMaxX div 2,GetMaxY div 2,
          'Congratulation! You have destroyed all enemy''s fleet');
        OutTextXY(GetMaxX div 2,GetMaxY div 2 + 20,'[ Ok ]');
        SetTextJustify(LeftText,TopText);
      end;
      Key:=ReadKey;
    until (Key=#13)or(Key=#27);
    Game2Caption;
    CompField.Hide:=False;
    DrawGrid(Field);
    DrawGrid(CompField);
    CompField.Hide:=True;
    ReadKey;
  end;
end;

end.