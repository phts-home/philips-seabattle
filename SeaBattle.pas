program SeaBattle;
uses
  Graph, Crt, uInterf, uGame;

var
  gd, gm: Integer;
  CurItem: Integer;
  Key, Key2: Char;

begin
  gd:=Detect;
  InitGraph(gd,gm,'');

  Field.x0:=100;
  Field.y0:=80;
  Field.Hide:=False;
  Field.id:=1;
  CompField.x0:=350;
  CompField.y0:=80;
  CompField.Hide:=True;
  CompField.id:=2;

  SoundEnable:=True;
  Clever:=True;

  ShowScreen;

  CurItem:=0;
  SetColor(15);
  MenuCaption;
  Menu(CurItem);
  while True do
  begin
    Key:=ReadKey;
    case Key of
      #0:
        begin
          Key2:=ReadKey;
          case Key2 of
            #59:                  { [F1] }
              begin
                ShowHelp;
                MenuCaption;
                Menu(CurItem);
              end;
            #72:                  { [Up] }
              begin
                Dec(CurItem);
                if CurItem < 0 then CurItem:=3;
                Menu(CurItem);
              end;
            #80:                  { [Down] }
              begin
                Inc(CurItem);
                if CurItem > 3 then CurItem:=0;
                Menu(CurItem);
              end;
          end;
        end;
      #13:                      { [Enter] }
        case CurItem of
          0:       {Start}
            begin
              InitGame;
              MenuCaption;
              Menu(CurItem);
            end;
          1:       {About}
            begin
              ShowOptions;
              MenuCaption;
              Menu(CurItem);
            end;
          2:       {Options}
            begin
              ShowHelp;
              MenuCaption;
              Menu(CurItem);
            end;
          3:       {Exit}
            begin
              CloseGraph;
              Exit;
            end;
        end;
    end;
  end;
end.